import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repair_shop/bloc/appointment/appointment.dart';
import 'package:repair_shop/repository/mocked_appointment_repo.dart';
import 'package:repair_shop/repository/mocked_time_slot_repo.dart';
import 'package:repair_shop/ui/service_screen.dart';

import 'bloc/timeSlot/time_slot.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AppointmentBloc>(
          create: (BuildContext context) =>
              AppointmentBloc(MockedAppointmentRepo())
                ..add(const AppointmentStarted()),
        ),
        BlocProvider<TimeSlotBloc>(
            create: (BuildContext context) =>
                TimeSlotBloc(timeSlotRepository: MockedTimeSlotRepo())
                  ..add(const TimeSlotStarted()))
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const ServiceScreen(),
      ),
    );
  }
}
