import 'package:equatable/equatable.dart';

class TimeSlot extends Equatable {
  final DateTime start;
  final DateTime end;

  const TimeSlot({required this.start, required this.end});

  @override
  List<Object?> get props => [start, end];
}
