import 'package:equatable/equatable.dart';

class Product extends Equatable {
  const Product({required this.id, required this.name, required this.price});

  final int id;
  final String name;
  final int price;

  @override
  List<Object> get props => [id, name, price];
}
