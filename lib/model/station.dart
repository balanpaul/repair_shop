import 'package:equatable/equatable.dart';

enum StationSortOptions { none, rating, price, duration }

class Station extends Equatable {
  final int id;
  final double rating;
  final String name;
  final int price;
  final int duration;
  final String profileUrl;

  const Station(
      {required this.profileUrl,
      required this.id,
      required this.rating,
      required this.name,
      required this.price,
      required this.duration});

  @override
  List<Object?> get props => [id, rating, name, price, duration];
}
