import 'package:repair_shop/model/product.dart';

class ProductItem extends Product {
  final int quantity;

  const ProductItem(
      {required this.quantity,
      required int id,
      required String name,
      required int price})
      : super(id: id, name: name, price: price);

  factory ProductItem.fromProduct(Product product, int quantity) {
    return ProductItem(
        quantity: quantity,
        id: product.id,
        name: product.name,
        price: product.price);
  }

  @override
  List<Object> get props => [id, name, price, quantity];
}
