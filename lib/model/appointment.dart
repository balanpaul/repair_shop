import 'package:repair_shop/model/product_item.dart';
import 'package:repair_shop/model/time_slot.dart';

class Appointment {
  final int? id;
  final List<ProductItem>? items;

  final String? name;
  final TimeSlot? slot;

  Appointment({this.id, this.name, this.slot, this.items});

  Appointment copyWith({id, name, slot, items}) {
    return Appointment(
        id: id ?? this.id,
        items: items ?? this.items,
        slot: slot ?? this.slot,
        name: name ?? this.name);
  }

  Appointment removeSlot() {
    return Appointment(id: id, items: items, slot: null, name: name);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Appointment &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          items == other.items &&
          slot == other.slot;

  @override
  int get hashCode => id.hashCode ^ items.hashCode ^ slot.hashCode;
}
