import 'package:repair_shop/bloc/utils.dart';
import 'package:repair_shop/model/appointment.dart';
import 'package:repair_shop/repository/appointment_repository.dart';

class MockedAppointmentRepo implements AppointmentRepository {
  final List<Appointment> _appointments = [];

  @override
  Appointment getAppointmentById(int id) {
//    return _appointments.firstWhere((element) => element.id == id);
    return _appointments.last;
  }

  @override
  void saveAppointment(Appointment appointment) {
    _appointments.add(appointment.copyWith(id: RandomGenerator.generateId()));
  }
}
