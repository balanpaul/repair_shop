import 'package:repair_shop/model/product_item.dart';

abstract class ProductItemRepository {
  void addItem(ProductItem item);

  bool removeItem(ProductItem item);

  List<ProductItem> loadCartItems();
}
