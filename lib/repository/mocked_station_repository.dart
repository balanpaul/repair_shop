import 'package:repair_shop/model/station.dart';
import 'package:repair_shop/repository/station_repository.dart';

class MockedStationRepository extends StationRepository {
  final List<Station> _stations = [
    const Station(
        profileUrl:
            "https://seeklogo.com/images/V/VW-logo-DE231313D6-seeklogo.com.png",
        id: 1,
        rating: 4.5,
        name: "VW",
        price: 300,
        duration: 2),
    const Station(
        profileUrl:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/BMW.svg/2048px-BMW.svg.png",
        id: 2,
        rating: 4.6,
        name: "BMW",
        price: 800,
        duration: 5),
    const Station(
        profileUrl:
            "https://e7.pngegg.com/pngimages/656/902/png-clipart-audi-car-honda-logo-gt4-european-series-text-trademark.png",
        id: 3,
        rating: 4.6,
        name: "Audi",
        price: 500,
        duration: 3),
    const Station(
        profileUrl:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Opel_logo.svg/2560px-Opel_logo.svg.png",
        id: 4,
        rating: 4,
        name: "Opel",
        price: 200,
        duration: 1),
    const Station(
        profileUrl:
            "https://toppng.com/uploads/preview/dacia-logo-vector-11574249425ozqdi5ta2t.png",
        id: 5,
        rating: 3,
        name: "Dacia",
        price: 100,
        duration: 2),
    const Station(
        profileUrl:
            "https://w7.pngwing.com/pngs/981/107/png-transparent-renault-logo-renault-symbol-jaguar-cars-peugeot-renault-angle-logo-car.png",
        id: 1,
        rating: 4.2,
        name: "Renault",
        price: 300,
        duration: 1),
    const Station(
        profileUrl:
            "https://toppng.com/uploads/preview/ford-logo-vector-11574248807jr4qm9cowi.png",
        id: 6,
        rating: 4.5,
        name: "Ford",
        price: 400,
        duration: 2),
    const Station(
        profileUrl:
            "https://seeklogo.com/images/V/VW-logo-DE231313D6-seeklogo.com.png",
        id: 7,
        rating: 5,
        name: "VW",
        price: 400,
        duration: 1),
  ];

  @override
  List<Station> getAllStations() {
    return _stations;
  }

  @override
  List<Station> searchAndSortBy(String term, StationSortOptions sort) {
    var list = _stations
        .where((element) => element.name.toLowerCase().contains(term))
        .toList();
    list.sort((a, b) => _compareStation(a, b, sort));
    return list;
  }

  int _compareStation(Station a, Station b, StationSortOptions sort) {
    switch (sort) {
      case StationSortOptions.rating:
        return a.rating.compareTo(b.rating);
      case StationSortOptions.price:
        return a.price.compareTo(b.price);
      case StationSortOptions.duration:
        return a.duration.compareTo(b.duration);
      case StationSortOptions.none:
        return 0;
    }
  }
}
