import 'package:repair_shop/model/time_slot.dart';
import 'package:repair_shop/repository/time_slot_repository.dart';

class MockedTimeSlotRepo implements TimeSlotRepository {
  List<TimeSlot> timeSlots = [
    TimeSlot(start: DateTime(2022, 3, 5, 10), end: DateTime(2022, 3, 5, 11)),
    TimeSlot(start: DateTime(2022, 3, 5, 12), end: DateTime(2022, 3, 5, 13)),
    TimeSlot(start: DateTime(2022, 3, 6, 12), end: DateTime(2022, 3, 6, 13))
  ];

  @override
  void addTimeSlot(TimeSlot slot) {
    return timeSlots.add(slot);
  }

  @override
  List<TimeSlot> getAllTimeSlots() {
    return timeSlots;
  }

  @override
  bool removeTimeSlot(TimeSlot slot) {
    return timeSlots.remove(slot);
  }
}
