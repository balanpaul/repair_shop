import 'package:repair_shop/model/appointment.dart';

abstract class AppointmentRepository{

  void saveAppointment(Appointment appointment);

  Appointment getAppointmentById(int id);


}