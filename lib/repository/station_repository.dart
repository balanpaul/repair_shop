import 'package:repair_shop/model/station.dart';

abstract class StationRepository {

  List<Station> getAllStations() ;


  List<Station> searchAndSortBy(String term, StationSortOptions sort);
}
