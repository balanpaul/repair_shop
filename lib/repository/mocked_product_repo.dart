import 'package:repair_shop/model/product.dart';
import 'package:repair_shop/repository/product_repository.dart';

class MockedProductRepository implements ProductRepository {
  final List<Product> _products = [
    const Product(id: 1, name: "Produs 1", price: 1000),
    const Product(id: 1, name: "Produs 2", price: 500),
    const Product(id: 2, name: "Test 1", price: 110),
    const Product(id: 2, name: "Piese", price: 230),
    const Product(id: 2, name: "Test 2", price: 540),
  ];

  @override
  void add(Product product) {
    return _products.add(product);
  }

  @override
  List<Product> getAllProducts() {
    return _products;
  }

  @override
  bool remove(Product product) {
    return _products.remove(product);
  }

  @override
  Future<List<Product>> searchProduct(String term) async {
    await Future.delayed(const Duration(milliseconds: 100));
    return _products
        .where((prod) => prod.name.toLowerCase().contains(term.trim()))
        .toList();
  }
}
