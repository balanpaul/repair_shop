import 'package:repair_shop/model/product_item.dart';
import 'package:repair_shop/repository/product_item_repository.dart';

class MockedProductItemRepo implements ProductItemRepository{

  final List<ProductItem> _items = [];


  @override
  void addItem(ProductItem item) {
     _items.add(item);
  }

  @override
  bool removeItem(ProductItem item) {
    return _items.remove(item);
  }

  @override
  List<ProductItem> loadCartItems() {

    return _items;
  }

}