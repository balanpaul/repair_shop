import 'package:repair_shop/model/time_slot.dart';

abstract class TimeSlotRepository {
  List<TimeSlot> getAllTimeSlots();

  void addTimeSlot(TimeSlot slot);

  bool removeTimeSlot(TimeSlot slot);

  
}
