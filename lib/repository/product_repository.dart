import 'package:repair_shop/model/product.dart';

abstract class ProductRepository{


  List<Product> getAllProducts();

  void add(Product  product);

  bool remove(Product product);

  Future<List<Product>> searchProduct(String term);
}