import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repair_shop/bloc/station/station.dart';
import 'package:repair_shop/repository/mocked_station_repository.dart';
import 'package:repair_shop/ui/widgets/appointment_view.dart';
import 'package:repair_shop/ui/widgets/header_list_stations.dart';
import 'package:repair_shop/ui/widgets/list_station.dart';

class ClientScreen extends StatelessWidget {
  const ClientScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) =>
          StationBloc(stationRepository: MockedStationRepository())
            ..add(const GetStation()),
      child: Scaffold(
        appBar: AppBar(
          elevation: 1,
          centerTitle: true,
          backgroundColor: Colors.white,
          leading: BackButton(
            color: Colors.black,
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          title: const AppointmentView(),
        ),
        body: Column(
          children: const [
            HeaderListStations(),
            Expanded(child: StationList())
          ],
        ),
      ),
    );
  }
}
