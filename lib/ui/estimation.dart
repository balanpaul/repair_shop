import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:repair_shop/bloc/cart/cart.dart';
import 'package:repair_shop/bloc/product/product.dart';
import 'package:repair_shop/model/product.dart';
import 'package:repair_shop/model/product_item.dart';
import 'package:repair_shop/repository/mocked_product_repo.dart';
import 'package:repair_shop/ui/widgets/autocomplete_product.dart';
import 'package:repair_shop/ui/widgets/table_items.dart';

class EstimationStep extends StatelessWidget {
  const EstimationStep({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ProductBloc>(
          create: (BuildContext context) =>
              ProductBloc(MockedProductRepository())..add(GetAllProduct()),
        ),
      ],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: const [
          Flexible(flex: 1, child: TableItems()),
          Flexible(flex: 1, child: AddItems()),
        ],
      ),
    );
  }
}

class AddItems extends StatefulWidget {
  const AddItems({Key? key}) : super(key: key);

  @override
  State<AddItems> createState() => _AddItemsState();
}

class _AddItemsState extends State<AddItems> {
  final TextEditingController _textEditingController = TextEditingController();

  Product? selected;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Flexible(
          flex: 2,
          child: AutocompleteProduct(onSelectedProduct: (product) {
            selected = product;
          }),
        ),
        Flexible(
          flex: 1,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            child: TextField(
              keyboardType: TextInputType.number,
              decoration: const InputDecoration(hintText: "cantitate"),
              controller: _textEditingController,
            ),
          ),
        ),
        buildAddButton(context)
      ],
    );
  }

  buildAddButton(BuildContext context) {
    return GestureDetector(
      child: const Icon(Icons.add),
      onTap: () {
        int? qty = int.tryParse(_textEditingController.text.trim());
        if (selected == null || qty == null) {
          Fluttertoast.showToast(
              msg: "Completati toate campurile",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.grey,
              textColor: Colors.white,
              fontSize: 16.0);
        } else {
          ProductItem productItem = ProductItem.fromProduct(selected!, qty);
          BlocProvider.of<CartBloc>(context).add(CartItemAdded(productItem));
        }
      },
    );
  }
}
