import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repair_shop/bloc/appointment/appointment.dart';
import 'package:repair_shop/bloc/timeSlot/time_slot.dart';
import 'package:repair_shop/bloc/utils.dart';
import 'package:repair_shop/model/time_slot.dart';

import 'package:syncfusion_flutter_calendar/calendar.dart';


class SchedulingStep extends StatelessWidget {
  const SchedulingStep({Key? key}) : super(key: key);
  final styleCellHour = const TextStyle(fontSize: 12);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TimeSlotBloc, TimeSlotState>(
      builder: ((context, state) {
        if (state is TimeSlotLoading) {
          return const CircularProgressIndicator();
        } else if (state is TimeSlotLoaded) {
          return buildCalendar(context, state.slots);
        }

        return const Text("Nu se poate incarca calendarul");
      }),
    );
  }

  SfCalendar buildCalendar(BuildContext context, List<TimeSlot> slots) {
    return SfCalendar(
      view: CalendarView.week,
      showDatePickerButton: true,
      minDate: DateTime.now(),
      showCurrentTimeIndicator: false,
      showWeekNumber: false,
      showNavigationArrow: true,
      firstDayOfWeek: 1,
      onSelectionChanged: (CalendarSelectionDetails calendarSelectionDetails) {
        final slot = TimeSlot(
            start: calendarSelectionDetails.date!,
            end: calendarSelectionDetails.date!.add(const Duration(hours: 1)));
        BlocProvider.of<TimeSlotBloc>(context).add(TimeSlotSelected(slot));
        BlocProvider.of<AppointmentBloc>(context)
            .add(AddSlotToAppointment(slot));
//        BlocProvider.of<CartBloc>(context).add(TimeSlotSelected(slot));
      },
      timeRegionBuilder: buildTimeRegion,
      specialRegions: _getTimeRegions(slots),
      timeSlotViewSettings: const TimeSlotViewSettings(
        dateFormat: 'd',
        dayFormat: 'EEE',
        timeInterval: Duration(hours: 1),
        startHour: 8,
        endHour: 17,
      ),
    );
  }

  Widget buildTimeRegion(
      BuildContext context, TimeRegionDetails timeRegionDetails) {
    return Opacity(
      opacity: 0.5,
      child: Container(
        color: Colors.grey[300],
        alignment: Alignment.center,
        child: Text(
          DateFormatUtils.convertToDateHm(timeRegionDetails.region.startTime),
          style: styleCellHour,
        ),
      ),
    );
  }

  List<TimeRegion> _getTimeRegions(List<TimeSlot> slots) {
    return slots
        .map((e) => TimeRegion(
            startTime: e.start,
            endTime: e.end,
            enablePointerInteraction: false,
            color: Colors.black54.withOpacity(0.4),
            iconData: const IconData(0xe33c, fontFamily: 'MaterialIcons')))
        .toList();
  }
}
