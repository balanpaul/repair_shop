import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:repair_shop/bloc/appointment/appointment.dart';
import 'package:repair_shop/bloc/cart/cart.dart';
import 'package:repair_shop/bloc/timeSlot/time_slot.dart';
import 'package:repair_shop/model/product_item.dart';
import 'package:repair_shop/repository/mocked_product_item_repo.dart';
import 'package:repair_shop/ui/colors.dart';
import 'package:repair_shop/ui/estimation.dart';
import 'package:repair_shop/ui/client_screen.dart';
import 'package:repair_shop/ui/scheduling.dart';
import 'package:repair_shop/ui/widgets/appointment_pop_up.dart';

class ServiceScreen extends StatefulWidget {
  const ServiceScreen({Key? key}) : super(key: key);

  @override
  _ServiceScreenState createState() => _ServiceScreenState();
}

class _ServiceScreenState extends State<ServiceScreen> {
  int _index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar:
            AppBar(backgroundColor: myColor, title: const Text("Liciteaza")),
        body: MultiBlocProvider(providers: [
          BlocProvider<CartBloc>(
              create: (BuildContext context) =>
                  CartBloc(productItemRepository: MockedProductItemRepo())
                    ..add(const CartStarted()))
        ], child: buildBody()));
  }

  Widget buildBody() {
    return Stepper(
      margin: EdgeInsets.zero,
      type: StepperType.horizontal,
      currentStep: _index,
      onStepCancel: stepCancel,
      onStepContinue: stepContinue,
      onStepTapped: (int index) {
        setState(() {
          _index = index;
        });
      },
      steps: <Step>[
        buildStepDeviz(),
        buildStepCalendar(),
      ],
      controlsBuilder: (BuildContext context, ControlsDetails controlsDetails) {
        return buildControlsStepper(controlsDetails, context);
      },
    );
  }

  Row buildControlsStepper(
      ControlsDetails controlsDetails, BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        cancelStep(controlsDetails),
        continueStep(controlsDetails, context)
      ],
    );
  }

  TextButton continueStep(
      ControlsDetails controlsDetails, BuildContext context) {
    return controlsDetails.currentStep != 1
        ? TextButton(
            onPressed: () {
              if (controlsDetails.currentStep == 0) {
                List<ProductItem> products =
                    (context.read<CartBloc>().state as CartLoaded).items;
                BlocProvider.of<AppointmentBloc>(context)
                    .add(AddProductItemToAppointment(products));
              }
              controlsDetails.onStepContinue!();
            },
            child: const Text('Inainte'))
        : TextButton(
            onPressed: () {
              var state = context.read<AppointmentBloc>().state;
              if ((state as AppointmentLoaded).appointment.slot == null) {
                Fluttertoast.showToast(
                    msg: "Selectati un timp",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.grey,
                    textColor: Colors.white,
                    fontSize: 16.0);
              } else {
                showAlertDialog(context);
              }
            },
            child: const Text('Creeaza'),
          );
  }

  Widget cancelStep(ControlsDetails controlsDetails) {
    return controlsDetails.currentStep == 0
        ? Container()
        : TextButton(
            onPressed: () {
              TimeSlotState state = context.read<TimeSlotBloc>().state;
              if (state is TimeSlotLoaded && state.lastAdded != null) {
                context
                    .read<TimeSlotBloc>()
                    .add(TimeSlotRemoved(state.lastAdded!));
                context
                    .read<AppointmentBloc>()
                    .add(const RemoveSlotToAppointment());
              }
              controlsDetails.onStepCancel!();
            },
            child: const Text('Inapoi'),
          );
  }

  Step buildStepCalendar() {
    return Step(
      isActive: _index == 1,
      title: const Text('Stabileste ora'),
      content: SizedBox(
          height: MediaQuery.of(context).size.height - 250,
          child: const SchedulingStep()),
    );
  }

  Step buildStepDeviz() {
    return Step(
      isActive: _index == 0,
      title: const Text('Creaza deviz'),
      content: SizedBox(
          height: MediaQuery.of(context).size.height - 250,
          child: const EstimationStep()),
    );
  }

  void stepCancel() {
    if (_index > 0) {
      setState(() {
        _index -= 1;
      });
    }
  }

  void stepContinue() {
    if (_index <= 0) {
      setState(() {
        _index += 1;
      });
    }
  }

  Future<void> showAlertDialog(BuildContext context) async {
    String? name = await showDialog(
        context: context, builder: (context) => AppointmentPopUp());
    if (name != null) {
      BlocProvider.of<AppointmentBloc>(context).add(SaveAppointment(name));
      Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) => const ClientScreen(),
      ));
    }
  }
}
