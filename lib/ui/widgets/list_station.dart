import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repair_shop/bloc/station/station.dart';
import 'package:repair_shop/model/station.dart';
import 'package:repair_shop/ui/widgets/station_card.dart';

class StationList extends StatelessWidget {
  const StationList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(2),
        child:
            BlocBuilder<StationBloc, StationState>(builder: ((context, state) {
          if (state.status == StationStateStatus.loading) {
            return const Center(child: CircularProgressIndicator());
          } else if (state.status == StationStateStatus.failure) {
            return const Text("Ooops, a aparut o eraore ");
          } else if (state.status == StationStateStatus.success &&
              state.stations.isEmpty) {
            return const Text("Niciun ofertant disponibil");
          }
          return buildList(context, state.stations);
        })));
  }

  Widget buildList(BuildContext context, List<Station> stations) {
    return ListView.builder(
        itemCount: stations.length,
        itemBuilder: (context, index) {
          return StationCard(
            station: stations[index],
          );
        });
  }
}
