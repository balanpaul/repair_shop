import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repair_shop/bloc/cart/cart.dart';
import 'package:repair_shop/model/product_item.dart';

class TableItems extends StatelessWidget {
  const TableItems({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: BlocBuilder<CartBloc, CartState>(
        builder: ((context, state) {
          if (state is CartLoading) {
            return const CircularProgressIndicator();
          } else if (state is CartLoaded) {
            return buildTable(state.items, context);
          }
          return const Text('Error!');
        }),
      ),
    );
  }

  Widget buildTable(List<ProductItem> items, BuildContext context) {
    return DataTable(
        columnSpacing: 32,
        dividerThickness: 1,
        columns: const [
          DataColumn(label: Text("Produs")),
          DataColumn(label: Text("Cantitate")),
          DataColumn(label: Text("Pret")),
          DataColumn(label: Text("Total")),
          DataColumn(label: Text("")),
        ],
        rows: items.map((e) => mapItemToRow(e, context)).toList());
  }

  DataRow mapItemToRow(ProductItem e, BuildContext context) {
    return DataRow(cells: [
      DataCell(Text(e.name)),
      DataCell(Text(e.quantity.toString())),
      DataCell(Text(e.price.toString())),
      DataCell(Text((e.price * e.quantity).toString())),
      DataCell(GestureDetector(
        child: const Icon(Icons.close),
        onTap: () {
          BlocProvider.of<CartBloc>(context).add(CartItemRemoved(e));
        },
      ))
    ]);
  }
}
