import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repair_shop/bloc/station/station.dart';
import 'package:repair_shop/model/station.dart';

///HeaderListStation contains search and sort option for list
class HeaderListStations extends StatelessWidget {
  const HeaderListStations({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 2),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            "Ofertanti",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
          ),
          _SearchBox(),
          const _SortHeader()
        ],
      ),
    );
  }
}

class _SearchBox extends StatefulWidget {
  @override
  __SearchBoxState createState() => __SearchBoxState();
}

class __SearchBoxState extends State<_SearchBox> {
  Timer? _debounce;

  @override
  void dispose() {
    _debounce?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width / 2,
      child: TextField(
        onChanged: _onSearchChanged,
        decoration: const InputDecoration(
            hintText: "Cauta Ofertanti", suffixIcon: Icon(Icons.search)),
      ),
    );
  }

  _onSearchChanged(String query) {
    if (_debounce != null && _debounce!.isActive) _debounce?.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      BlocProvider.of<StationBloc>(context).add(FilterStation(query.trim()));
      // do something with query
    });
  }
}

class _SortHeader extends StatefulWidget {
  const _SortHeader({Key? key}) : super(key: key);

  @override
  _SortHeaderState createState() => _SortHeaderState();
}

class _SortHeaderState extends State<_SortHeader> {
  StationSortOptions keySort = StationSortOptions.none;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          "Sortare dupa:",
          style: TextStyle(color: Colors.black38, fontWeight: FontWeight.bold),
        ),
        const SizedBox(
          width: 8,
        ),
        buildSortOptions()
      ],
    );
  }

  buildSortOptions() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        buildOption("Rating", StationSortOptions.rating),
        buildOption("Pret", StationSortOptions.price),
        buildOption("Timp", StationSortOptions.duration),
      ],
    );
  }

  buildOption(String text, StationSortOptions sort) {
    return TextButton(
      onPressed: () {
        BlocProvider.of<StationBloc>(context).add(SortStationEvent(sort));
        setState(() {
          keySort = sort;
        });
      },
      child: Text(
        text,
        style: TextStyle(
            color: keySort == sort ? Colors.lightBlue : Colors.black54),
      ),
    );
  }
}
