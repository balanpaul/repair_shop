import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repair_shop/bloc/appointment/appointment.dart';
import 'package:repair_shop/bloc/utils.dart';
import 'package:repair_shop/model/appointment.dart';

//Appointment overview
class AppointmentView extends StatefulWidget {
  const AppointmentView({Key? key}) : super(key: key);

  @override
  State<AppointmentView> createState() => _AppointmentViewState();
}

class _AppointmentViewState extends State<AppointmentView> {
  @override
  void initState() {
    BlocProvider.of<AppointmentBloc>(context).add(const GetAppointment(1));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppointmentBloc, AppointmentState>(
        builder: ((context, state) {
      if (state is AppointmentLoading) {
        return const CircularProgressIndicator();
      } else if (state is AppointmentCreated) {
        return buildContentAppointment(state.appointment);
      }
      return const Text("Ooops");
    }));
  }

  Widget buildContentAppointment(Appointment appointment) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          appointment.name!,
          style:
              const TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
        ),
        Text(
          DateFormatUtils.convertDateToDayHm(appointment.slot!.start),
          style: const TextStyle(color: Colors.black38, fontSize: 16),
        )
      ],
    );
  }
}
