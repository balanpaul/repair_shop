import 'package:flutter/material.dart';
import 'package:repair_shop/bloc/product/product.dart';
import 'package:repair_shop/model/product.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

typedef AutocompleteOnSelectedProduct = void Function(Product option);

///Autocomplete for search a product
class AutocompleteProduct extends StatelessWidget {
  final AutocompleteOnSelectedProduct? onSelectedProduct;

  const AutocompleteProduct({Key? key, this.onSelectedProduct})
      : super(key: key);

  static String _displayStringForOption(Product option) => option.name;

  @override
  Widget build(BuildContext context) {
    return buildAutocomplete(context);
  }

  Autocomplete<Product> buildAutocomplete(BuildContext context) {
    return Autocomplete<Product>(
      fieldViewBuilder:
          ((context, textEditingController, focusNode, onFieldSubmitted) {
        return TextField(
          controller: textEditingController,
          focusNode: focusNode,
          decoration: const InputDecoration(
              hintText: "Rulmeant", suffixIcon: Icon(Icons.search)),
        );
      }),
      displayStringForOption: _displayStringForOption,
      optionsBuilder: (TextEditingValue textEditingValue) async {
        return BlocProvider.of<ProductBloc>(context)
            .filter(textEditingValue.text);
      },
      onSelected: (Product selection) {
        if (onSelectedProduct != null) {
          onSelectedProduct!(selection);
        }
      },
    );
  }
}
