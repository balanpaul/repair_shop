import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:repair_shop/model/station.dart';
//representation of card for a @class Station
class StationCard extends StatelessWidget {
  final Station station;

  const StationCard({Key? key, required this.station}) : super(key: key);

  TextStyle greyInfoStyle() =>
      const TextStyle(color: Colors.black38, fontSize: 14);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            Expanded(flex: 5, child: buildContentPart()),
            Expanded(flex: 1, child: buildPhoto())
          ],
        ),
      ),
    );
  }

  Column buildContentPart() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [buildTitle(), buildDuration(), buildPrice()],
    );
  }

  Widget buildPhoto() {
    return CachedNetworkImage(
      imageUrl: station.profileUrl,
      fit: BoxFit.fitHeight,
      errorWidget: (context, url, error) => const Icon(Icons.error),
      placeholder: (context, url) => Container(
        padding: const EdgeInsets.all(8),
        alignment: Alignment.center,
        child: const CircularProgressIndicator(),
      ),
    );
  }

  Widget buildTitle() {
    return Row(
      children: [
        Text(
          station.name,
          overflow: TextOverflow.ellipsis,
          style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        const Spacer(
          flex: 1,
        ),
        const Icon(
          Icons.star,
          color: Colors.yellow,
        ),
        Text(
          station.rating.toStringAsFixed(2),
          style: greyInfoStyle(),
        ),
        const SizedBox(
          width: 8,
        )
      ],
    );
  }

  buildDuration() {
    return Text(
      "Durata: ${station.duration} ore",
      style: greyInfoStyle(),
    );
  }

  buildPrice() {
    return Align(
        alignment: Alignment.bottomLeft,
        child: Text("Cost estimat ${station.price} RON"));
  }
}
