import 'package:flutter/material.dart';

///pop up for save an appointment
class AppointmentPopUp extends StatelessWidget {
  AppointmentPopUp({Key? key}) : super(key: key);

  final TextEditingController _nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text("Salvare"),
      content: SizedBox(
        width: 200,
        child: buildTextField(),
      ),
      actions: [
        TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text("Anulare")),
        TextButton(
            onPressed: () {
              Navigator.pop(context, _nameController.text.trim());
            },
            child: const Text("Salvare"))
      ],
    );
  }

  buildTextField() {
    return TextField(
      controller: _nameController,
      decoration: const InputDecoration(hintText: "Numele programarii"),
    );
  }
}
