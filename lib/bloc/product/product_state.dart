import 'package:equatable/equatable.dart';
import 'package:repair_shop/model/product.dart';

abstract class ProductState extends Equatable {
  const ProductState();

  @override
  List<Object> get props => [];
}

class FilteredProductInProgress extends ProductState {}

//State for product which
class FilteredProductsLoaded extends ProductState {
  final List<Product> filteredProducts;

  const FilteredProductsLoaded({required this.filteredProducts});

  @override
  List<Object> get props => [filteredProducts];
}

///State for error
class FilteredProductError extends ProductState {}