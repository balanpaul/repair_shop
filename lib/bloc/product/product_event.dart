import 'package:equatable/equatable.dart';

abstract class ProductEvent extends Equatable {
  const ProductEvent();

  @override
  List<Object> get props => [];
}

///Event for get all product from repository
class GetAllProduct extends ProductEvent {}

///Filter a [Product] by [searchTerm]
class FilteredProduct extends ProductEvent {
  final String searchTerm;

  const FilteredProduct(this.searchTerm);

  @override
  List<Object> get props => [searchTerm];

  @override
  String toString() {
    return 'FilteredProduct{searchTerm: $searchTerm}';
  }
}
