import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:repair_shop/bloc/product/product_event.dart';
import 'package:repair_shop/bloc/product/product_state.dart';
import 'package:repair_shop/model/product.dart';
import 'package:repair_shop/repository/product_repository.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  final ProductRepository _productRepository;

  ProductBloc(this._productRepository) : super(FilteredProductInProgress()) {
    on<FilteredProduct>(_filterProducts);
    on<GetAllProduct>(_getAllProducts);
  }

  void _filterProducts(
      FilteredProduct event, Emitter<ProductState> emit) async {
    try {
      final items = await _productRepository.searchProduct(event.searchTerm);
      emit(FilteredProductsLoaded(filteredProducts: items));
    } catch (_) {
      emit(FilteredProductError());
    }
  }

  Future<List<Product>> filter(String search) {
    return _productRepository.searchProduct(search);
  }

  FutureOr<void> _getAllProducts(
      GetAllProduct event, Emitter<ProductState> emit) {
    final List<Product> items = _productRepository.getAllProducts();
    emit(FilteredProductsLoaded(filteredProducts: items));
  }
}
