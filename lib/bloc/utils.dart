import 'dart:math';

import 'package:intl/intl.dart';

class RandomGenerator {
  /// Return a random int between 1 and 9999
  static int generateId() {
    final Random rng = Random();
    return rng.nextInt(9999);
  }
}

class DateFormatUtils {
  /// Return a string which represent a datetime in hour:minutes format
  static String convertToDateHm(DateTime dateTime) {
    final DateFormat dateFormat = DateFormat.Hm();
    return dateFormat.format(dateTime);
  }

  //Return a short complete date
  static String convertDateToDayHm(DateTime dateTime) {
    final DateFormat dateFormat = DateFormat("dd.MM.yyyy HH:mm");
    return dateFormat.format(dateTime);
  }
}
