import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repair_shop/bloc/timeSlot/time_slot_event.dart';
import 'package:repair_shop/bloc/timeSlot/time_slot_state.dart';
import 'package:repair_shop/repository/time_slot_repository.dart';

class TimeSlotBloc extends Bloc<TimeSlotEvent, TimeSlotState> {
  TimeSlotBloc({required this.timeSlotRepository})
      : super(const TimeSlotLoading()) {
    on<TimeSlotStarted>(_onStarted);
    on<TimeSlotSelected>(_onItemSelected);
    on<TimeSlotRemoved>(_onItemRemoved);
  }

  final TimeSlotRepository timeSlotRepository;

  FutureOr<void> _onStarted(
      TimeSlotStarted event, Emitter<TimeSlotState> emit) {
    emit(const TimeSlotLoading());
    try {
      final slots = timeSlotRepository.getAllTimeSlots();
      emit(TimeSlotLoaded(slots: slots));
    } catch (_) {
      emit(const TimeSlotError());
    }
  }

  FutureOr<void> _onItemSelected(
      TimeSlotSelected event, Emitter<TimeSlotState> emit) {
    final state = this.state;
    if (state is TimeSlotLoaded) {
      try {
        timeSlotRepository.addTimeSlot(event.timeSlot);
        emit(TimeSlotLoaded(
            slots: [...state.slots, event.timeSlot],
            lastAdded: event.timeSlot));
      } catch (_) {
        emit(const TimeSlotError());
      }
    }
  }

  FutureOr<void> _onItemRemoved(
      TimeSlotRemoved event, Emitter<TimeSlotState> emit) {
    final state = this.state;
    if (state is TimeSlotLoaded) {
      try {
        timeSlotRepository.removeTimeSlot(event.timeSlot);
        emit(
          TimeSlotLoaded(slots: [...state.slots]..remove(event.timeSlot)),
        );
      } catch (_) {
        emit(const TimeSlotError());
      }
    }
  }
}
