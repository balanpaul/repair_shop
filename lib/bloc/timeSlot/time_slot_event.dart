import 'package:equatable/equatable.dart';
import 'package:repair_shop/model/time_slot.dart';

abstract class TimeSlotEvent extends Equatable {
  const TimeSlotEvent();
}
///First event after a bloc is created
class TimeSlotStarted extends TimeSlotEvent {
  const TimeSlotStarted();

  @override
  List<Object> get props => [];
}

/// Select a [TimeSlot] from Calendar
class TimeSlotSelected extends TimeSlotEvent {
  const TimeSlotSelected(this.timeSlot);

  final TimeSlot timeSlot;

  @override
  List<Object> get props => [timeSlot];
}

///Remove a [TimeSlot] form Calendar
class TimeSlotRemoved extends TimeSlotEvent {
  const TimeSlotRemoved(this.timeSlot);

  final TimeSlot timeSlot;

  @override
  List<Object> get props => [timeSlot];
}
