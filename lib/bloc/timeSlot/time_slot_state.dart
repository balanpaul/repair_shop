import 'package:equatable/equatable.dart';
import 'package:repair_shop/model/time_slot.dart';

abstract class TimeSlotState extends Equatable {
  const TimeSlotState();

  @override
  List<Object> get props => [];
}

class TimeSlotLoading extends TimeSlotState {
  const TimeSlotLoading();

  @override
  List<Object> get props => [];
}
///TimeSlotState for loaded [slots]
///Contains:
///   - list of [TimeSlot]
///   - last added [TimeSlot]
class TimeSlotLoaded extends TimeSlotState {
  final List<TimeSlot> slots;
  final TimeSlot? lastAdded;

   const TimeSlotLoaded({required this.slots, this.lastAdded});

  @override
  List<Object> get props => [slots];
}

///State for error
class TimeSlotError extends TimeSlotState {
  const TimeSlotError();

  @override
  List<Object> get props => [];
}