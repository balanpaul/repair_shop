import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repair_shop/bloc/cart/cart_event.dart';
import 'package:repair_shop/bloc/cart/cart_state.dart';
import 'package:repair_shop/repository/product_item_repository.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartBloc({required this.productItemRepository}) : super(CartLoading()) {
    on<CartStarted>(_onStarted);
    on<CartItemAdded>(_onItemAdded);
    on<CartItemRemoved>(_onItemRemoved);
  }

  final ProductItemRepository productItemRepository;

  FutureOr<void> _onStarted(CartStarted event, Emitter<CartState> emit) {
    emit(CartLoading());
    try {
      final items = productItemRepository.loadCartItems();
      emit(CartLoaded(items: [...items]));
    } catch (_) {
      emit(CartError());
    }
  }

  FutureOr<void> _onItemAdded(CartItemAdded event, Emitter<CartState> emit) {
    final state = this.state;
    if (state is CartLoaded) {
      try {
        productItemRepository.addItem(event.item);
        emit(CartLoaded(items: [...state.items, event.item]));
      } catch (_) {
        emit(CartError());
      }
    }
  }

  FutureOr<void> _onItemRemoved(
      CartItemRemoved event, Emitter<CartState> emit) {
    final state = this.state;
    if (state is CartLoaded) {
      try {
        productItemRepository.removeItem(event.item);
        emit(
          CartLoaded(
            items: [...state.items]..remove(event.item),
          ),
        );
      } catch (_) {
        emit(CartError());
      }
    }
  }
}
