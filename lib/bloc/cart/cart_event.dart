import 'package:equatable/equatable.dart';
import 'package:repair_shop/model/product_item.dart';

abstract class CartEvent extends Equatable {
  const CartEvent();

  @override
  List<Object> get props => [];
}

///First event after a bloc is created
class CartStarted extends CartEvent {
  const CartStarted();

  @override
  List<Object> get props => [];
}

///Add a [ProductItem] to cart state
class CartItemAdded extends CartEvent {
  const CartItemAdded(this.item);

  final ProductItem item;

  @override
  List<Object> get props => [item];
}

///Remove a [ProductItem] from cart state
class CartItemRemoved extends CartEvent {
  const CartItemRemoved(this.item);

  final ProductItem item;

  @override
  List<Object> get props => [item];
}
