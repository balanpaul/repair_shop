import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:repair_shop/model/product_item.dart';

@immutable
abstract class CartState extends Equatable {
  const CartState();
}

///State for cart loading
class CartLoading extends CartState {
  @override
  List<Object> get props => [];
}

///State for cart loaded with success.
///Contains a list of [ProductItem]
class CartLoaded extends CartState {
  const CartLoaded({required this.items});

  final List<ProductItem> items;

  @override
  List<Object> get props => [items];
}

///State for error
class CartError extends CartState {
  @override
  List<Object> get props => [];
}
