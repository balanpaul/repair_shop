import 'package:equatable/equatable.dart';
import 'package:repair_shop/model/appointment.dart';

abstract class AppointmentState extends Equatable {
  const AppointmentState();

  @override
  List<Object> get props => [];
}

///State for Loading
class AppointmentLoading extends AppointmentState {
  const AppointmentLoading();

  @override
  List<Object> get props => [];
}

///State for appointment loaded with success
class AppointmentLoaded extends AppointmentState {
  final Appointment appointment;

  const AppointmentLoaded(this.appointment);

  @override
  List<Object> get props => [appointment];
}

///State after a appointment is saved with success
class AppointmentCreated extends AppointmentState {
  final Appointment appointment;

  const AppointmentCreated(this.appointment);

  @override
  List<Object> get props => [appointment];
}
///Error State
class AppointmentError extends AppointmentState {
  const AppointmentError();

  @override
  List<Object> get props => [];
}
