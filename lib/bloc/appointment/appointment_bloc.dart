import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repair_shop/bloc/appointment/appointment_event.dart';
import 'package:repair_shop/bloc/appointment/appointment_state.dart';
import 'package:repair_shop/model/appointment.dart';
import 'package:repair_shop/repository/appointment_repository.dart';

class AppointmentBloc extends Bloc<AppointmentEvent, AppointmentState> {
  final AppointmentRepository _appointmentRepository;

  AppointmentBloc(this._appointmentRepository)
      : super(const AppointmentLoading()) {
    on<AppointmentStarted>(_startAppointment);
    on<AddProductItemToAppointment>(_addProductItem);
    on<AddSlotToAppointment>(_addSlot);
    on<SaveAppointment>(_saveAppointment);
    on<GetAppointment>(_getAppointment);
    on<RemoveSlotToAppointment>(_removeSlot);
  }

  ///creation of bloc
  FutureOr<void> _startAppointment(
      AppointmentStarted event, Emitter<AppointmentState> emit) {
    emit(AppointmentLoaded(Appointment()));
  }

  ///add products items to an appointment
  FutureOr<void> _addProductItem(
      AddProductItemToAppointment event, Emitter<AppointmentState> emit) {
    final currentState = state;
    if (currentState is AppointmentLoaded) {
      Appointment appointment =
          currentState.appointment.copyWith(items: event.productsItems);
      emit(AppointmentLoaded(appointment));
    }
  }

  FutureOr<void> _addSlot(
      AddSlotToAppointment event, Emitter<AppointmentState> emit) {
    final currentState = state;

    if (currentState is AppointmentLoaded) {
      Appointment appointment =
          currentState.appointment.copyWith(slot: event.timeSlot);
      emit(AppointmentLoaded(appointment));
    }
  }

  FutureOr<void> _saveAppointment(
      SaveAppointment event, Emitter<AppointmentState> emit) {
    final currentState = state;

    if (currentState is AppointmentLoaded) {
      Appointment appointment =
          currentState.appointment.copyWith(name: event.name);
      _appointmentRepository.saveAppointment(appointment);
      emit(AppointmentLoaded(appointment));
    }
  }

  FutureOr<void> _getAppointment(
      GetAppointment event, Emitter<AppointmentState> emit) {
    final Appointment app = _appointmentRepository.getAppointmentById(event.id);
    emit(AppointmentCreated(app));
  }

  FutureOr<void> _removeSlot(
      RemoveSlotToAppointment event, Emitter<AppointmentState> emit) {
    final currentState = state;

    if (currentState is AppointmentLoaded) {
      Appointment appointment = currentState.appointment.removeSlot();
      emit(AppointmentLoaded(appointment));
    }
  }
}
