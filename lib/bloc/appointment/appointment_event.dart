import 'package:equatable/equatable.dart';
import 'package:repair_shop/model/product_item.dart';
import 'package:repair_shop/model/time_slot.dart';

abstract class AppointmentEvent extends Equatable {
  const AppointmentEvent();

  @override
  List<Object> get props => [];
}

///Add a list of [ProductItem] to appointment
class AddProductItemToAppointment extends AppointmentEvent {
  final List<ProductItem> productsItems;

  const AddProductItemToAppointment(this.productsItems);

  @override
  List<Object> get props => [productsItems];
}

///first event after a bloc is created
class AppointmentStarted extends AppointmentEvent {
  const AppointmentStarted();

  @override
  List<Object> get props => [];
}

/// Add a [TimeSlot] to appointment
class AddSlotToAppointment extends AppointmentEvent {
  final TimeSlot timeSlot;

  const AddSlotToAppointment(this.timeSlot);

  @override
  List<Object> get props => [timeSlot];
}

///Remove a [TimeSlot] from appointment
class RemoveSlotToAppointment extends AppointmentEvent {
  const RemoveSlotToAppointment();

  @override
  List<Object> get props => [];
}

///Save appointment in repository
class SaveAppointment extends AppointmentEvent {
  final String name;

  const SaveAppointment(this.name);

  @override
  List<Object> get props => [name];
}

///Get Appointment by id from repository
class GetAppointment extends AppointmentEvent {
  final int id;

  const GetAppointment(this.id);

  @override
  List<Object> get props => [id];
}
