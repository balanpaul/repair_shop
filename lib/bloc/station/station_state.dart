import 'package:equatable/equatable.dart';
import 'package:repair_shop/model/station.dart';

enum StationStateStatus { initial, loading, success, failure }

///State for stations screen.
///Contains details about:
///   - [status] -> StationStateStatus of bloc
///   - [stations] -> list of stations loaded or filtered
///   - [stationSort] -> latest sort options chosen
///   - [searchTerm] -> latest searchTerm use
class StationState extends Equatable {
  final StationStateStatus status;
  final List<Station> stations;
  final StationSortOptions stationSort;
  final String? searchTerm;

  const StationState(
      {this.status = StationStateStatus.initial,
      this.stations = const [],
      this.stationSort = StationSortOptions.none,
      this.searchTerm});

  StationState copyWith(
      {StationStateStatus? status,
      List<Station>? stations,
      StationSortOptions? stationSort,
      String? searchTerm}) {
    return StationState(
        status: status ?? this.status,
        stations: stations ?? this.stations,
        stationSort: stationSort ?? this.stationSort,
        searchTerm: searchTerm ?? this.searchTerm);
  }

  @override
  List<Object> get props => [stations, status, stationSort, searchTerm ?? ""];

  @override
  String toString() {
    return 'StationState{status: $status, stations: $stations, stationSort: $stationSort, searchTerm: $searchTerm}';
  }
}
