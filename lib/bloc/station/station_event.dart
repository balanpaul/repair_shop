import 'package:equatable/equatable.dart';
import 'package:repair_shop/model/station.dart';

abstract class StationEvent extends Equatable {
  const StationEvent();

  @override
  List<Object> get props => [];
}

///Event for get all available stations
class GetStation extends StationEvent {
  const GetStation();

  @override
  List<Object> get props => [];
}

///Filter [Station] by [term]
class FilterStation extends StationEvent {
  final String term;

  const FilterStation(this.term);

  @override
  List<Object> get props => [term];
}

/// Ssort [Station} by one of available options [StationSortOptions]
class SortStationEvent extends StationEvent {
  final StationSortOptions sortStation;

  const SortStationEvent(this.sortStation);

  @override
  List<Object> get props => [sortStation];
}
