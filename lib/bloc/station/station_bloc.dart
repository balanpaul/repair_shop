import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repair_shop/bloc/station/station_event.dart';
import 'package:repair_shop/bloc/station/station_state.dart';
import 'package:repair_shop/repository/station_repository.dart';

class StationBloc extends Bloc<StationEvent, StationState> {
  StationBloc({required this.stationRepository}) : super(const StationState()) {
    on<GetStation>(_getStations);
    on<FilterStation>(_filterStations);
    on<SortStationEvent>(_sortStations);
  }

  final StationRepository stationRepository;

  FutureOr<void> _getStations(GetStation event, Emitter<StationState> emit) {
    final crtState = state;
    emit(crtState.copyWith(status: StationStateStatus.loading));
    try {
      final stations = stationRepository.getAllStations();
      emit(
          crtState.copyWith(stations: stations, status: StationStateStatus.success));
    } catch (_) {
      emit(crtState.copyWith(status: StationStateStatus.failure));
    }
  }

  FutureOr<void> _filterStations(
      FilterStation event, Emitter<StationState> emit) {
    final crtState = state;
    emit(crtState.copyWith(status: StationStateStatus.loading));
    try {
      final stations =
          stationRepository.searchAndSortBy(event.term, crtState.stationSort);
      emit(crtState.copyWith(
          stations: stations,
          status: StationStateStatus.success,
          searchTerm: event.term));
    } catch (_) {
      emit(crtState.copyWith(status: StationStateStatus.failure));
    }
  }

  FutureOr<void> _sortStations(
      SortStationEvent event, Emitter<StationState> emit) {
    final crtState = state;
    emit(crtState.copyWith(status: StationStateStatus.loading));
    try {
      final stations = stationRepository.searchAndSortBy(
          crtState.searchTerm ?? "", event.sortStation);
      var copyWith = crtState.copyWith(
          stations: stations,
          status: StationStateStatus.success,
          stationSort: event.sortStation);
      emit(copyWith);
    } catch (_) {
      emit(crtState.copyWith(status: StationStateStatus.failure));
    }
  }
}
